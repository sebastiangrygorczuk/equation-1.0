/*
CoVar, Coefficient Variable, is a class for a variable that has two parts, a double number and a char for what double stands for.
For example 13C stands for constant with a coefficient of 13, similarly 2X is a variable X with a coefficient of 2.
*/

#ifndef COVAR_H
#define COVAR_H

class CoVar
{
    public:
        /*
        Constructor sets nVariables to 0; Coefficient to 0 and VariableName to 'C'
        */
        CoVar();
        /*
        A function that sets the nVariables
        */
        void setN(int nVariable);
        /*
        A functions that asks the user to enter the coefficient of the CoVar
        */
        void setCoVar();
        /*
        A function that prints out the CoVar variable
        */
        void printCoVar();
        /*
        Provides the user with the Variable Name
        */
        char getVar();
        /*
        Provides the user with the Coefficient of the Variable 
        */
        double getCo();
        /*
        Allows user to use the + operator between the CoVar 
        */
        CoVar operator+(CoVar Passed);
        /*
        Allows user to use the * operator between the CoVar 
        */
        CoVar operator*(CoVar Passed);

    private:
        /*
        The amount of variables that the program is working with
        */
        int nVariables;
        /*
        The numerical coefficient of the variables 
        */
        double Coefficient;
        /*
        The character which identify it as constant or variable and which variable 
        */
        char VariableName;
};

#endif // COVAR_H