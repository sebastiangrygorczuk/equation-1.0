#include "CoVar.h"
#include <iostream>
using namespace std;

CoVar::CoVar()
{
nVariables = 0;
Coefficient = 'C';
VariableName = 0;
}

void CoVar :: setN(int nVariable)
{
    nVariables = nVariable;
}

void CoVar :: setCoVar()
{
    double Co = 0;
    double temp = 0;
    char Var = 'C';
    cout << "Are you inputting fraction or an integer? (F/I)" << endl;
setJump0:
    cin >> Var;
    if(Var == 'F')
    {
        cout << "What is the numerator?" << endl;
        cin >> temp;
        cout << "What is the denominator?" << endl;
        cin >> Co;
        Coefficient = temp/Co;
    }
    else if (Var == 'I')
    {
        cout << "Input the coefficient: " << endl;
        cin >> Co;
        Coefficient = Co;
    }
    else
    {
        cout << "Sorry I did not understand please enter F for fractions or I for integers." << endl;
        goto setJump0;
    }
    switch(nVariables)
    {
        case 0:
        {
            VariableName = 'C';
            break;
        }
        case 1:
        {
            cout << "This input is Constant (C) or Variable (X)?" << endl;
setJump1:
            cin >> Var;
            if(Var == 'C')
            {
                VariableName = Var;
                break;
            }
            else if(Var == 'X')
            {
                VariableName = Var;
                break;
            }
            else
            {
                cout << "I did not understand, please enter C for Constant or X for Variable" << endl;
                goto setJump1;
            }
        }
        case 2:
        {
            cout << "This input is Constant (C) or Variable (X,Y)" << endl;
setJump2:
            cin >> Var;
            if(Var == 'C')
            {
                VariableName = Var;
                break;
            }
            else if(Var == 'X' || Var == 'Y')
            {
                VariableName = Var;
                break;
            }
            else
            {
                cout << "I did not understand, please enter C for Constant or X,Y for Variable" << endl;
                goto setJump2;
            }
        }
        case 3:
        {
            cout << "This input is Constant (C) or Variable (X,Y,Z)" << endl;
setJump3:
            cin >> Var;
            if(Var == 'C')
            {
                VariableName = Var;
                break;
            }
            else if((Var == 'X') || (Var == 'Y') || (Var = 'Z'))
            {
                VariableName = Var;
                break;
            }
            else
            {
                cout << "I did not understand, please enter C for Constant or  X,Y,Z for Variable" << endl;
                goto setJump3;
            }
        }
    }
}

void CoVar :: printCoVar ()
{
    cout << Coefficient;
    if(VariableName == 'C')
    {
        cout << "";
    }
    else
    {
        cout << VariableName;
    }
}

char CoVar :: getVar()
{
    return VariableName;
}

double CoVar :: getCo()
{
    return Coefficient;
}

CoVar CoVar :: operator+(CoVar Passed)
    {
        CoVar temp;
        try
        {
            if(VariableName == Passed.VariableName)
            {
                temp.Coefficient = Coefficient + Passed.Coefficient;
                return temp;
            }
            else
            {
                throw 0;
            }
        }
        catch (...)
        {
            cout << "The two inputs are different Variables and can't be added" << endl;
        }
    }

CoVar CoVar :: operator*(CoVar Passed)
{
    CoVar temp;
    try
    {
        if(VariableName == Passed.VariableName)
        {
            temp.Coefficient = Coefficient * Passed.Coefficient;
            return temp;
        }
        else
        {
            throw 0;
        }
    }
    catch (int x)
    {
        cout << "The two inputs are different Variables and can't be added" << endl;
    }
}