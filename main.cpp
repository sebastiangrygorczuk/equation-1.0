/*
Simplify Equation
By: Sebastian Grygorczuk

This program gathers inputs with up to 3 variables and sums up each of the variables with the constants on the left and the variables on the right.
*/

#include <iostream>
#include "CoVar.h"
using namespace std;

/*
Simple is where all of the meat of the program happens it will ask you how many variables you use, after that it allows you to input the Left Hand side of the Equation then the Right Hand Side once you've done that the program prints out all the variables with their coefficients summed.
*/
void simple();

int main()
{
    char input = 'N';
    cout << "Welcome to Simplify, we will take your inputs!" << endl;
mainJump1:
    simple();
    cout << "Would you like to simplify another equation? (Y/N)" << endl;
    cin >> input;
    if(input == 'Y')
    {
        goto mainJump1;
    }
    else if (input == 'N')
    {
        return 0;
    }
    else
    {
        cout << "Sorry, I did not understand please enter Y for Yes or N for No." << endl;
    }

    return 0;
}

void simple()
{
    int inputInt = 0;
    char inputChar = 'C';
    int nVars;
    double Sums [4] = {0,0,0,0};
    int LHSSize = 0;
    int RHSSize = 0;
    cout << "How many variables are you using (1-3)" << endl;
simpleJump1:
    cin >> inputInt;
    if(inputInt == 1 || inputInt == 2 || inputInt == 3)
    {
        nVars = inputInt;
    }
    else
    {
        cout << "Sorry I did not understand, please input a number between 0 and 3" << endl;
        goto simpleJump1;
    }
    cout << "How many inputs will you have on the Left Hand Side?" << endl;

    cin >> inputInt;
    LHSSize = inputInt;
    CoVar LHS [LHSSize];
    for (int i = 0; i < LHSSize; i++)
    {
        LHS[i].setN(nVars);
        LHS[i].setCoVar();
        if(LHS[i].getVar() == 'C')
        {
            Sums[0] = Sums[0] + LHS[i].getCo();
        }
        else if(LHS[i].getVar() == 'X')
        {
            Sums[1] = Sums[1] + -1*LHS[i].getCo();
        }
        else if(LHS[i].getVar() == 'Y')
        {
            Sums[2] = Sums[2] + -1*LHS[i].getCo();
        }
        else
        {
            Sums[3] = Sums[3] + -1*LHS[i].getCo();
        }
    }

    cout << "This is the Left Hand Side you've input: " << endl;

    for(int i = 0; i < LHSSize; i++)
    {
        LHS[i].printCoVar();
        if(i < LHSSize-1)
        {
            cout << " + ";
        }
        else
        {
            cout << " = " << endl;
        }
    }

    cout << "How many inputs will you have on the Right Hand Side?" << endl;

    cin >> inputInt;
    RHSSize = inputInt;
    CoVar RHS [RHSSize];
    for (int i = 0; i < RHSSize; i++)
    {
        RHS[i].setN(nVars);
        RHS[i].setCoVar();
        if(RHS[i].getVar() == 'C')
        {
            Sums[0] = Sums[0] + -1*RHS[i].getCo();
        }
        else if(RHS[i].getVar() == 'X')
        {
            Sums[1] = Sums[1] + RHS[i].getCo();
        }
        else if(LHS[i].getVar() == 'Y')
        {
            Sums[2] = Sums[2] + RHS[i].getCo();
        }
        else
        {
            Sums[3] = Sums[3] + RHS[i].getCo();
        }
    }

    cout << "This is the Right Hand Side you've input:" << endl;
    cout << " = ";
    for(int i = 0; i < RHSSize; i++)
    {
        RHS[i].printCoVar();
        if(i < RHSSize-1)
        {
            cout << " + ";
        }
    }

    cout << "This is your full equation: " << endl;

    for(int i = 0; i < LHSSize; i++)
    {
        LHS[i].printCoVar();
        if(i < LHSSize-1)
        {
            cout << " + ";
        }
        else
        {
            cout << " = ";
        }
    }
    for(int i = 0; i < RHSSize; i++)
    {
        RHS[i].printCoVar();
        if(i < RHSSize-1)
        {
            cout << " + ";
        }
    }
    cout << endl;
    cout << "And this is the simplifed version of it: " << endl;
    for(int i = 0; i < nVars+1; i++)
    {
        cout << Sums[i];
        if(i == 0)
        {
            cout  << " = ";
        }
        else if(i == 1)
        {
            cout << "X";
        }
        else if(i == 2)
        {
            cout << "Y";
        }
        else
        {
            cout << "Z";
        }
        if(i < nVars && i != 0)
        {
            cout << " + ";
        }
    }
    cout << endl;

}